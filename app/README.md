# Redukto

Redukto is a LAN file sharing app compatible with [Dukto](https://www.msec.it/blog/dukto/).

Features:
- Sending and receiving files and folders
- Sending and receiving plaintext messages
- Integration in file, image and text quick share menus
- Running in background (toggled by a quick tile)

No encryption or authentication is applied, so this app should only be used in trusted networks.
