package org.bitbucket.asgusev.redukto.android.service

import android.annotation.SuppressLint
import android.app.*
import android.app.Notification.FOREGROUND_SERVICE_IMMEDIATE
import android.content.*
import android.net.ConnectivityManager
import android.net.Network
import android.net.NetworkCapabilities
import android.net.NetworkRequest
import android.os.Binder
import android.os.Build
import android.os.IBinder
import androidx.annotation.RequiresApi
import androidx.core.app.NotificationCompat
import androidx.core.app.NotificationManagerCompat
import androidx.preference.PreferenceManager
import org.bitbucket.asgusev.redukto.R
import org.bitbucket.asgusev.redukto.android.ui.ReduktoTileService
import org.bitbucket.asgusev.redukto.protocol.*
import java.io.IOException
import java.lang.NullPointerException
import java.net.BindException
import java.net.InetAddress
import java.net.NetworkInterface
import java.nio.ByteBuffer
import java.util.NoSuchElementException
import java.util.concurrent.Executors
import java.util.concurrent.locks.ReentrantLock


@SuppressLint("ResourceType")
class ReduktoService : Service() {
    @Volatile
    private var protocol: DuktoProtocol? = null
    private val avatarServer: AvatarServer by lazy {
        AvatarServer(resources.openRawResource((R.drawable.avatar)).readBytes())
    }
    private val executorControl = Executors.newSingleThreadExecutor()
    private val executorTransfer = Executors.newCachedThreadPool()
    private val peersList = mutableListOf<Peer>()
    private var peerListUpdateListener: ((List<Peer>) -> Unit)? = null
    private var stateUpdateListener: ((Boolean) -> Unit)? = null
    @Volatile
    private var transfers = 0
    @Volatile
    private var bound = 0
    @Volatile
    private var manuallyStarted = false
    private val shouldRun: Boolean
        get() = manuallyStarted || bound > 0 || transfers > 0
    private var running = false
    private val addresses: List<String>
        get() = protocol?.localAddresses?.map { it.toString().substring(1) } ?: emptyList()
    private val startStopLock = ReentrantLock()
    private val connectivityManager by lazy { getSystemService(CONNECTIVITY_SERVICE) as ConnectivityManager }

    inner class ReduktoServiceBinder: Binder() {
        val peersList: List<Peer>
            get() = this@ReduktoService.peersList
        val signature
            get() = protocol?.signature

        fun send(items: List<TransferItem>, target: String, port: Short = DuktoProtocol.DEFAULT_PORT) {
            ++transfers
            createBackgroundNotification()
            executorTransfer.submit {
                try {
                    protocol?.send(items, target, port, sendingListener)
                } finally {
                    --transfers
                }
                checkStopProtocol()
            }
        }

        fun setPeersListUpdatesListener(listener: (List<Peer>) -> Unit) {
            peerListUpdateListener = listener
        }

        fun unsetPeersListUpdatesListener() { peerListUpdateListener = null }

        fun setStateUpdateListener(listener: (Boolean) -> Unit) {
            stateUpdateListener = listener
        }

        fun unsetStateUpdateListener() { stateUpdateListener = null }

        fun update() {
            this@ReduktoService.peersList.clear()
            peerListUpdateListener?.invoke(peersList)
            executorControl.submit { protocol?.restart() }
        }
    }

    private val sendingListener: SendingListener by lazy {
        object: SendingListener {
            private val notificationBuilder by lazy {
                NotificationCompat.Builder(this@ReduktoService, NOTIFICATION_CHANNEL_OUTGOING_TRANSFER)
                    .setSmallIcon(R.drawable.ic_stat)
                    .setColor(getColor(R.color.colorAccent))
                    .setContentTitle(getString(R.string.msg_sending_data))
            }
            private val notificationId = System.currentTimeMillis().toInt()
            private val notificationManager by lazy { NotificationManagerCompat.from(this@ReduktoService) }
            private var lastUpdateTime = System.currentTimeMillis()

            init {
                notificationManager.notify(notificationId, notificationBuilder.build())
            }

            override fun onProgress(bytesSent: Long, totalBytes: Long) {
                val curTime = System.currentTimeMillis()
                if (curTime > lastUpdateTime + NOTIFICATION_UPDATE_TIMEOUT) {
                    notificationBuilder.setProgress(totalBytes.toInt(), bytesSent.toInt(), false)
                    notificationManager.notify(notificationId, notificationBuilder.build())
                    lastUpdateTime = curTime
                }
            }

            override fun onError(e: Exception?) {
                val errorMessage =
                    getString(R.string.msg_failed).format(
                        if (e != null && e::class.java == java.nio.channels.UnresolvedAddressException::class.java)
                            getString(R.string.msg_unknown_target)
                        else
                            e?.localizedMessage
                    )

                notificationBuilder.setContentText(errorMessage)
                notificationManager.notify(notificationId, notificationBuilder.build())
            }

            override fun onFinish() {
                notificationManager.cancel(notificationId)
            }
        }
    }

    override fun onBind(intent: Intent): IBinder {
        ++bound
        executorControl.submit(::startProtocol).get()
        return ReduktoServiceBinder()
    }

    override fun onRebind(intent: Intent?) {
        ++bound
        executorControl.submit(::startProtocol).get()
    }

    override fun onUnbind(intent: Intent?): Boolean {
        --bound
        executorControl.submit(::checkStopProtocol).get()
        return true
    }

    private fun makeChannels() {
        val notificationManager = getSystemService(Context.NOTIFICATION_SERVICE) as NotificationManager
        val notificationChannels = listOf(
            NotificationChannel(
                NOTIFICATION_CHANNEL_BG,
                getString(R.string.notification_channel_bg_service),
                NotificationManager.IMPORTANCE_LOW
            ),
            NotificationChannel(
                NOTIFICATION_CHANNEL_INCOMING_TRANSFER,
                getString(R.string.notification_channel_incoming),
                NotificationManager.IMPORTANCE_LOW
            ),
            NotificationChannel(
                NOTIFICATION_CHANNEL_OUTGOING_TRANSFER,
                getString(R.string.notification_channel_outgoing),
                NotificationManager.IMPORTANCE_LOW
            )
        )
        notificationChannels.forEach { notificationManager.createNotificationChannel(it) }
    }

    override fun onCreate() {
        super.onCreate()
        makeChannels()
    }

    override fun onStartCommand(intent: Intent?, flags: Int, startId: Int): Int {
        when (intent?.action) {
            ACTION_START -> executorControl.submit(::startProtocol).get()
            ACTION_STOP -> executorControl.submit(::checkStopProtocol).get()
            ACTION_TOGGLE -> {
                manuallyStarted = !manuallyStarted
                if (manuallyStarted)
                    executorControl.submit(::startProtocol).get()
                else
                    executorControl.submit(::checkStopProtocol).get()
                if (intent.getBooleanExtra(EXTRA_TOGGLE_TILE, false)) {
                    val tileIntent = Intent(this, ReduktoTileService::class.java).apply {
                        action = ReduktoTileService.ACTION_TOGGLE
                        putExtra(ReduktoTileService.EXTRA_ACTIVE, shouldRun)
                    }
                    startService(tileIntent)
                }
            }
        }
        return START_NOT_STICKY
    }

    private fun stopImpl() {
        val notification = NotificationCompat.Builder(this, NOTIFICATION_CHANNEL_BG)
            .setSmallIcon(R.drawable.ic_stat)
            .setColor(getColor(R.color.colorAccent))
            .setContentTitle(getString(R.string.msg_stopping))
            .build()
        startForeground(1, notification)
        protocol?.stop()
        protocol = null
        avatarServer.stop()
        peersList.clear()
        stateUpdateListener?.invoke(false)
    }

    private fun startImpl() {
        val networkInterfaces = NetworkInterface.getNetworkInterfaces().toList()
            .filter { it.name.startsWith("wlan") }
        if (networkInterfaces.isNotEmpty()) {
            avatarServer.start()
            try {
                val deviceName = Build.MODEL
                val userName = PreferenceManager.getDefaultSharedPreferences(this)
                    .getString(getString(R.string.preference_key_username), null)
                    ?: getString(R.string.default_username)
                protocol = DuktoProtocol(
                    listener = serviceProtocolListener, networkInterfaces = networkInterfaces,
                    userName = userName, deviceName = deviceName, osName = OS_NAME
                )
                stateUpdateListener?.invoke(true)
            } catch (e: NoSuchElementException) { avatarServer.stop()
            } catch (e: BindException) { avatarServer.stop()
            } catch (e: NullPointerException) { avatarServer.stop() }
        }
        createBackgroundNotification()
    }

    private val networkAvailabilityCallback = object: ConnectivityManager.NetworkCallback() {
        override fun onAvailable(network: Network) = startImpl()

        override fun onLost(network: Network) {
            stopImpl()
            createBackgroundNotification()
        }
    }

    private fun checkStopProtocol() {
        startStopLock.lock()
        if (shouldRun || !running) {
            if (shouldRun)
                createBackgroundNotification()
            startStopLock.unlock()
            return
        }

        connectivityManager.unregisterNetworkCallback(networkAvailabilityCallback)
        stopImpl()
        stopForeground(STOP_FOREGROUND_REMOVE)
        running = false
        startStopLock.unlock()
    }

    private fun startProtocol() {
        startStopLock.lock()
        if (running) {
            createBackgroundNotification()
            startStopLock.unlock()
            return
        }

        running = true
        createBackgroundNotification()
        val networkRequest = NetworkRequest.Builder()
            .addTransportType(NetworkCapabilities.TRANSPORT_WIFI)
            .build()
        connectivityManager.requestNetwork(networkRequest, networkAvailabilityCallback)
        startStopLock.unlock()
    }

    private fun createBackgroundNotification() {
        val notificationBuilder = NotificationCompat.Builder(this, NOTIFICATION_CHANNEL_BG)
            .setSmallIcon(R.drawable.ic_stat)
            .setColor(getColor(R.color.colorAccent))
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.S)
            notificationBuilder.foregroundServiceBehavior = FOREGROUND_SERVICE_IMMEDIATE

        if (protocol != null)
            notificationBuilder
                .setContentTitle(getString(R.string.msg_running).format(protocol?.signature?.let { shortenName(it) }))
                .setContentText(getString(R.string.msg_address).format(addresses.joinToString(", ")))
        else
            notificationBuilder.setContentTitle(getString(R.string.msg_connection_fail))

        if (bound == 0 && transfers == 0) {
            val stopIntent = Intent(this, ReduktoService::class.java).apply {
                action = ACTION_TOGGLE
                putExtra(EXTRA_TOGGLE_TILE, true)
            }
            val pendingIntent = PendingIntent.getService(
                this@ReduktoService, 0, stopIntent, PendingIntent.FLAG_IMMUTABLE)
            notificationBuilder.addAction(R.drawable.ic_stat, getString(R.string.stop), pendingIntent)
        }
        startForeground(1, notificationBuilder.build())
    }

    private fun showNotification(id: Int, builder: NotificationCompat.Builder) =
        NotificationManagerCompat.from(this).notify(id, builder.build())

    private val serviceProtocolListener by lazy {
        object : ProtocolListener {
            private val savingPolicy = getSavingPolicy(this@ReduktoService)

            override fun onNewPeer(peer: Peer) {
                peersList.add(peer)
                peerListUpdateListener?.invoke(peersList)
            }

            override fun onLeftPeer(peer: Peer) {
                peersList.remove(peer)
                peerListUpdateListener?.invoke(peersList)
            }

            override fun onMessageReceived(message: String, authorIP: InetAddress, authorName: String?) {
                val copyIntent = Intent(this@ReduktoService, CopyService::class.java).apply {
                    action = CopyService.ACTION_COPY
                    putExtra(CopyService.EXTRA_CONTENT, message)
                }
                val pendingIntent =
                    PendingIntent.getService(
                        this@ReduktoService, message.hashCode(), copyIntent, PendingIntent.FLAG_IMMUTABLE)
                val builder = NotificationCompat.Builder(this@ReduktoService, NOTIFICATION_CHANNEL_INCOMING_TRANSFER)
                    .setSmallIcon(R.drawable.ic_stat)
                    .setColor(getColor(R.color.colorAccent))
                    .setContentTitle(getString(R.string.msg_incoming_text).format(authorRepresentation(authorName, authorIP)))
                    .setContentText(message)
                    .addAction(R.drawable.ic_stat, getString(R.string.msg_copy), pendingIntent)
                showNotification(message.hashCode(), builder)
            }

            override fun onDirectoryDeclaration(name: String, authorIP: InetAddress, authorName: String?) =
                savingPolicy.declareDir(name)

            @RequiresApi(Build.VERSION_CODES.Q)
            override fun onFileReception(
                fileName: String, fileSize: Long, authorIP: InetAddress, authorName: String?
            ): ReceptionListener {
                val notificationManager = NotificationManagerCompat.from(this@ReduktoService)
                val notificationId = fileName.hashCode()
                val notificationBuilder = NotificationCompat.Builder(
                    this@ReduktoService, NOTIFICATION_CHANNEL_INCOMING_TRANSFER
                )
                    .setSmallIcon(R.drawable.ic_stat)
                    .setColor(getColor(R.color.colorAccent))
                    .setContentTitle(getString(R.string.file_notification_title).format(
                        fileName, authorRepresentation(authorName, authorIP)))
                    .setContentText(getString(R.string.msg_reception))
                    .setProgress(fileSize.toInt(), 0, false)
                fun showError(error: Exception?) {
                    notificationBuilder
                        .setContentText(getString(R.string.msg_failed).format(error?.localizedMessage))
                        .setProgress(0, 0, false)
                    notificationManager.notify(notificationId, notificationBuilder.build())
                }

                var failed = false
                val fileWriter: FileWriter? = try{
                    savingPolicy.getFileWriter(fileName)
                } catch (e: IOException) {
                    failed = true
                    showError(e)
                    null
                }

                notificationManager.notify(notificationId, notificationBuilder.build())
                var lastUpdateTime = System.currentTimeMillis()

                return object : ReceptionListener {
                    override fun onProgress(newBytes: ByteBuffer, bytesReceived: Long, totalBytes: Long) {
                        if (failed || fileWriter == null)
                            return
                        try {
                            fileWriter.onProgress(newBytes)
                            val curTime = System.currentTimeMillis()
                            if (curTime > lastUpdateTime + NOTIFICATION_UPDATE_TIMEOUT) {
                                notificationBuilder.setProgress(fileSize.toInt(), bytesReceived.toInt(), false)
                                notificationManager.notify(notificationId, notificationBuilder.build())
                                lastUpdateTime = curTime
                            }
                        } catch (error: IOException) {
                            showError(error)
                            failed = true
                        }
                    }

                    override fun onError(e: Exception?) {
                        fileWriter?.onFailure()
                        showError(e)
                        failed = true
                    }

                    override fun onFinish() {
                        if (failed || fileWriter == null)
                            return
                        try {
                            fileWriter.onFinish()
                        } catch (error: IOException) {
                            showError(error)
                            failed = true
                            return
                        }

                        notificationManager.cancel(notificationId)
                        val fileUri = fileWriter.fileUri
                        val openIntent = Intent().apply {
                            action = Intent.ACTION_VIEW
                            data = fileUri
                            flags = Intent.FLAG_GRANT_READ_URI_PERMISSION
                        }
                        val pendingIntent = PendingIntent.getActivity(
                            this@ReduktoService, fileUri.hashCode(), openIntent, PendingIntent.FLAG_IMMUTABLE)
                        notificationBuilder
                            .setContentText(getString(R.string.msg_reception_success))
                            .setProgress(0, 0, false)
                            .setAutoCancel(true)
                            .setContentIntent(pendingIntent)
                        notificationManager.notify(notificationId, notificationBuilder.build())
                    }
                }
            }

            override fun onError(e: Throwable?) {
                val notificationBuilder = NotificationCompat.Builder(
                    this@ReduktoService, NOTIFICATION_CHANNEL_INCOMING_TRANSFER
                )
                    .setSmallIcon(R.drawable.ic_stat)
                    .setColor(getColor(R.color.colorAccent))
                    .setContentTitle(getString(R.string.msg_error).format(e?.localizedMessage))
                val notificationId = System.currentTimeMillis().toInt()
                showNotification(notificationId, notificationBuilder)
            }
        }
    }

    companion object {
        const val ACTION_START = "START"
        const val ACTION_STOP = "STOP"
        const val ACTION_TOGGLE = "TOGGLE"
        const val NOTIFICATION_CHANNEL_BG = "backgroundService"
        const val NOTIFICATION_CHANNEL_INCOMING_TRANSFER = "incomingTransfer"
        const val NOTIFICATION_CHANNEL_OUTGOING_TRANSFER = "outgoingTransfer"
        const val EXTRA_TOGGLE_TILE = "toggle_tile"
        const val OS_NAME = "Android"
        const val NOTIFICATION_UPDATE_TIMEOUT = 200
    }
}
