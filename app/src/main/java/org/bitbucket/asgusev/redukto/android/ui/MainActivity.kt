package org.bitbucket.asgusev.redukto.android.ui

import android.app.*
import android.content.*
import android.content.pm.PackageManager
import android.os.Build
import android.os.Bundle
import android.view.*
import android.widget.*
import androidx.activity.result.contract.ActivityResultContracts
import androidx.core.app.ActivityCompat
import androidx.core.content.ContextCompat
import androidx.documentfile.provider.DocumentFile
import androidx.fragment.app.DialogFragment
import androidx.recyclerview.widget.RecyclerView
import org.bitbucket.asgusev.redukto.R
import org.bitbucket.asgusev.redukto.android.service.ReduktoService
import org.bitbucket.asgusev.redukto.android.service.UriFileItem
import org.bitbucket.asgusev.redukto.android.service.checkAddress
import org.bitbucket.asgusev.redukto.android.service.collectDirectory
import org.bitbucket.asgusev.redukto.protocol.Peer
import org.bitbucket.asgusev.redukto.protocol.TextItem
import org.bitbucket.asgusev.redukto.protocol.TransferItem


class MainActivity : BoundActivity(), ActivityCompat.OnRequestPermissionsResultCallback {
    @Volatile private var fileTransferTarget: String? = null
    private lateinit var peerSelector: PeerSelectionView

    private fun launchSendTextDialog(targetAddress: String) {
        val binder = this@MainActivity.serviceConnection.binder
        if (binder != null)
            SendTextDialog(targetAddress, binder)
                .show(supportFragmentManager, "sendTextDialog")
        else
            Toast.makeText(this, R.string.connection_failure, Toast.LENGTH_SHORT).show()
    }

    override fun updatePeersList(newPeerList: List<Peer>) =
        runOnUiThread {
            peersList.clear()
            peersList.addAll(newPeerList)
            peerSelector.update()
        }

    private fun showSendingDialog(address: String, userName: String?) {
        if (checkAddress(address)) {
            val dialog = ActionSelectionDialog(userName, address, this@MainActivity)
            dialog.show(supportFragmentManager, "actionSelectionDialog")
        } else {
            Toast.makeText(this, R.string.bad_ip, Toast.LENGTH_SHORT).show()
        }
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        val peersListView = findViewById<RecyclerView>(R.id.peersList)
        peerSelector = PeerSelectionView(peersListView, this, peersList, ::showSendingDialog)
        checkPermissions()
    }

    private fun requestWritePermission() {
        requestPermissions(arrayOf(WRITE_PERMISSION), WRITE_PERMISSION_REQUEST_CODE)
    }

    private fun requestNotificationsPermission() {
        requestPermissions(arrayOf(NOTIFICATIONS_PERMISSION), NOTIFICATIONS_PERMISSION_REQUEST_CODE)
    }

    private fun checkPermissions() {
        if (Build.VERSION.SDK_INT < Build.VERSION_CODES.R &&
            ContextCompat.checkSelfPermission(this, WRITE_PERMISSION) == PackageManager.PERMISSION_DENIED) {
            if (shouldShowRequestPermissionRationale(WRITE_PERMISSION)) {
                val rationaleDialogBuilder = AlertDialog.Builder(this)
                    .setTitle(R.string.permission_request_title)
                    .setMessage(R.string.write_permission_request_message)
                    .setPositiveButton(R.string.ok) { _, _ -> requestWritePermission() }
                    .setOnDismissListener { requestWritePermission() }
                val rationaleDialog = rationaleDialogBuilder.create()
                rationaleDialog.show()
            } else {
                requestWritePermission()
            }
        }
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.TIRAMISU &&
            ContextCompat.checkSelfPermission(this, NOTIFICATIONS_PERMISSION) == PackageManager.PERMISSION_DENIED) {
            if (shouldShowRequestPermissionRationale(NOTIFICATIONS_PERMISSION)) {
                val rationaleDialogBuilder = AlertDialog.Builder(this)
                    .setTitle(R.string.permission_request_title)
                    .setMessage(R.string.notifications_permission_request_message)
                    .setPositiveButton(R.string.ok) { _, _ -> requestNotificationsPermission() }
                    .setOnDismissListener { requestNotificationsPermission() }
                val rationaleDialog = rationaleDialogBuilder.create()
                rationaleDialog.show()
            } else {
                requestNotificationsPermission()
            }
        }
    }

    override fun onRequestPermissionsResult(requestCode: Int, permissions: Array<out String>, grantResults: IntArray) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults)
        if (permissions.isNotEmpty() && grantResults[0] == PackageManager.PERMISSION_DENIED) {
            val toastTextCode = when (requestCode) {
                WRITE_PERMISSION_REQUEST_CODE -> R.string.write_permission_request_message
                NOTIFICATIONS_PERMISSION_REQUEST_CODE -> R.string.notifications_permission_denied_message
                else -> throw IllegalArgumentException(getString(R.string.permission_error_text))
            }
            Toast.makeText(this, toastTextCode, Toast.LENGTH_LONG).show()
        }
    }

    override fun onResume() {
        super.onResume()
        peerSelector.loadAddress()
    }

    override fun onCreateOptionsMenu(menu: Menu?): Boolean {
        menuInflater.inflate(R.menu.action_menu_main, menu)
        return super.onCreateOptionsMenu(menu)
    }

    override fun onOptionsItemSelected(item: MenuItem) =
        when (item.itemId) {
            R.id.menu_refresh -> {
                serviceConnection.binder?.update()
                true
            }
            R.id.menu_settings -> {
                startActivity(Intent(this, SettingsActivity::class.java))
                true
            }
            else -> super.onOptionsItemSelected(item)
        }

    private fun send(transferItems: List<TransferItem>) {
        serviceConnection.binder?.send(transferItems, fileTransferTarget!!)
        fileTransferTarget = null
        keepBound = false
    }

    private val getFiles = registerForActivityResult(ActivityResultContracts.GetMultipleContents()) { uris ->
        val transferItems = uris.map { UriFileItem(it, this) }
        send(transferItems)
    }

    private val getDirectory = registerForActivityResult(ActivityResultContracts.OpenDocumentTree()) { uri ->
        val transferItems = collectDirectory(DocumentFile.fromTreeUri(this, uri!!)!!, this)
        send(transferItems)
    }

    private fun requestFileToSend(transferAddress: String) {
        fileTransferTarget = transferAddress
        keepBound = true
        getFiles.launch("*/*")
    }

    private fun requestFolderToSend(targetAddress: String) {
        fileTransferTarget = targetAddress
        keepBound = true
        getDirectory.launch(null)
    }

    class ActionSelectionDialog(private val peerName: String?, private val peerAddress: String,
                                private val context: MainActivity
    ): DialogFragment() {
        private val actionNames = arrayOf(
            context.getString(R.string.send_text),
            context.getString(R.string.send_file),
            context.getString(R.string.send_folder)
        )

        override fun onCreateDialog(savedInstanceState: Bundle?): AlertDialog =
            AlertDialog.Builder(context)
                .setTitle(peerName ?: peerAddress)
                .setItems(actionNames) { _, index ->
                    when (index) {
                        INDEX_SEND_TEXT -> context.launchSendTextDialog(peerAddress)
                        INDEX_SEND_FILE -> context.requestFileToSend(peerAddress)
                        INDEX_SEND_FOLDER -> context.requestFolderToSend(peerAddress)
                    }
                }
                .create()

        companion object {
            private const val INDEX_SEND_TEXT = 0
            private const val INDEX_SEND_FILE = 1
            private const val INDEX_SEND_FOLDER = 2
        }
    }

    class SendTextDialog(private val target: String, private val binder: ReduktoService.ReduktoServiceBinder): DialogFragment() {
        override fun onCreateDialog(savedInstanceState: Bundle?): Dialog =
            AlertDialog.Builder(context)
                .setTitle(getString(R.string.msg_send_to).format(target))
                .apply {
                    val textField = EditText(context)
                    setView(textField)
                    setPositiveButton(R.string.send) { _, _ ->
                        binder.send(listOf(TextItem(textField.text.toString())), target)
                    }
                }
                .create()
    }

    override fun onPause() {
        super.onPause()
        peerSelector.saveAddress()
    }

    companion object {
        const val WRITE_PERMISSION_REQUEST_CODE = 1
        const val NOTIFICATIONS_PERMISSION_REQUEST_CODE = 2
        const val WRITE_PERMISSION = "android.permission.WRITE_EXTERNAL_STORAGE"
        const val NOTIFICATIONS_PERMISSION = "android.permission.POST_NOTIFICATIONS"
    }
}
