package org.bitbucket.asgusev.redukto.android.service

import android.app.Service
import android.content.ClipData
import android.content.ClipboardManager
import android.content.Context
import android.content.Intent
import android.widget.Toast
import org.bitbucket.asgusev.redukto.R

class CopyService : Service() {
    override fun onBind(p0: Intent?) = null

    override fun onStartCommand(intent: Intent?, flags: Int, startId: Int): Int {
        val message = intent?.getStringExtra(EXTRA_CONTENT)
        if (message == null) {
            Toast.makeText(this, getString(R.string.msg_copy_fail), Toast.LENGTH_SHORT).show()
            return START_NOT_STICKY
        }

        val clipboard = this.getSystemService(Context.CLIPBOARD_SERVICE) as ClipboardManager
        val messageClip = ClipData.newPlainText("dkt_msg", message)
        clipboard.setPrimaryClip(messageClip)
        Toast.makeText(this, getString(R.string.msg_copy_success).format(message), Toast.LENGTH_SHORT).show()
        return START_NOT_STICKY
    }

    companion object {
        const val ACTION_COPY = "COPY"
        const val EXTRA_CONTENT = "CONTENT"
    }
}
