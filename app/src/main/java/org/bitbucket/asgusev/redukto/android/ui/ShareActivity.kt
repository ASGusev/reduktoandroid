package org.bitbucket.asgusev.redukto.android.ui

import android.content.Intent
import android.net.Uri
import android.os.Build
import android.os.Bundle
import android.view.Menu
import android.view.MenuItem
import android.widget.*
import androidx.recyclerview.widget.RecyclerView
import org.bitbucket.asgusev.redukto.R
import org.bitbucket.asgusev.redukto.android.service.UriFileItem
import org.bitbucket.asgusev.redukto.android.service.checkAddress
import org.bitbucket.asgusev.redukto.protocol.Peer
import org.bitbucket.asgusev.redukto.protocol.TextItem
import org.bitbucket.asgusev.redukto.protocol.TransferItem


class ShareActivity : BoundActivity() {
    private lateinit var peerSelector: PeerSelectionView
    private lateinit var items: List<TransferItem>

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_share)

        @Suppress("DEPRECATION")
        items = when {
            intent.action == Intent.ACTION_SEND &&
                    intent.hasExtra(Intent.EXTRA_STREAM) &&
                    Build.VERSION.SDK_INT >= Build.VERSION_CODES.TIRAMISU ->
                listOf(UriFileItem(intent.getParcelableExtra(Intent.EXTRA_STREAM, Uri::class.java)!!, this))

            intent.action == Intent.ACTION_SEND &&
                    intent.hasExtra(Intent.EXTRA_STREAM) &&
                    Build.VERSION.SDK_INT <= Build.VERSION_CODES.S ->
                listOf(UriFileItem(intent.getParcelableExtra(Intent.EXTRA_STREAM)!!, this))

            intent.action == Intent.ACTION_SEND && intent.hasExtra(Intent.EXTRA_TEXT) ->
                listOf(TextItem(intent.getStringExtra(Intent.EXTRA_TEXT)!!))

            intent.action == Intent.ACTION_SEND_MULTIPLE &&
                    intent.hasExtra(Intent.EXTRA_STREAM) &&
                    Build.VERSION.SDK_INT >= Build.VERSION_CODES.TIRAMISU ->
                intent.getParcelableArrayListExtra(Intent.EXTRA_STREAM, Uri::class.java)!!
                    .map { UriFileItem(it, this) }

            intent.action == Intent.ACTION_SEND_MULTIPLE &&
                    intent.hasExtra(Intent.EXTRA_STREAM) &&
                    Build.VERSION.SDK_INT <= Build.VERSION_CODES.S ->
                intent.getParcelableArrayListExtra<Uri>(Intent.EXTRA_STREAM)!!
                    .map { UriFileItem(it, this) }

            intent.action == Intent.ACTION_SEND_MULTIPLE && intent.hasExtra(Intent.EXTRA_TEXT) ->
                intent.getStringArrayListExtra(Intent.EXTRA_TEXT)!!.map { TextItem(it) }
            else -> listOf()
        }

        val peersListView = findViewById<RecyclerView>(R.id.peersList)
        peerSelector = PeerSelectionView(peersListView, this, peersList) { address, _ ->
            if (checkAddress(address)) {
                serviceConnection.binder?.send(items, address)
                this.finish()
            } else {
                Toast.makeText(this, R.string.bad_ip, Toast.LENGTH_SHORT).show()
            }
        }
    }

    override fun updatePeersList(newPeerList: List<Peer>) =
        runOnUiThread {
            peersList.clear()
            peersList.addAll(newPeerList)
            peerSelector.update()
        }

    override fun onCreateOptionsMenu(menu: Menu?): Boolean {
        menuInflater.inflate(R.menu.action_menu_share, menu)
        return super.onCreateOptionsMenu(menu)
    }

    override fun onOptionsItemSelected(item: MenuItem) =
        when (item.itemId) {
            R.id.menu_refresh -> {
                serviceConnection.binder?.update()
                true
            }
            else -> super.onOptionsItemSelected(item)
        }

    override fun onDestroy() {
        super.onDestroy()
        peerSelector.saveAddress()
    }
}
