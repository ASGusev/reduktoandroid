package org.bitbucket.asgusev.redukto.android.ui

import android.content.ComponentName
import android.content.Context
import android.content.Intent
import android.content.ServiceConnection
import android.os.IBinder
import androidx.appcompat.app.AppCompatActivity
import org.bitbucket.asgusev.redukto.R
import org.bitbucket.asgusev.redukto.android.service.ReduktoService
import org.bitbucket.asgusev.redukto.protocol.shortenName
import org.bitbucket.asgusev.redukto.protocol.Peer


abstract class BoundActivity: AppCompatActivity() {
    protected val serviceConnection = Connection()
    protected var peersList = mutableListOf<Peer>()
    protected var keepBound = false

    protected inner class Connection: ServiceConnection {
        var binder: ReduktoService.ReduktoServiceBinder? = null

        override fun onServiceConnected(p0: ComponentName?, p1: IBinder?) {
            binder = p1 as ReduktoService.ReduktoServiceBinder?
            binder?.setStateUpdateListener(::updateTitle)
            binder?.setPeersListUpdatesListener(::updatePeersList)
            title = binder?.signature?.let { shortenName(it) } ?: getString(R.string.msg_connection_fail)
            binder?.let { updatePeersList(it.peersList) }
        }

        override fun onServiceDisconnected(p0: ComponentName?) {
            binder?.unsetPeersListUpdatesListener()
            binder?.unsetStateUpdateListener()
            binder = null
        }
    }

    protected abstract fun updatePeersList(newPeerList: List<Peer>)

    private fun updateTitle(running: Boolean) =
        runOnUiThread {
            if (running)
                serviceConnection.binder?.signature?.let { title = shortenName(it) }
            else
                title = getString(R.string.msg_connection_fail)
        }

    override fun onResume() {
        super.onResume()
        if (!keepBound) {
            val startingIntent = Intent(this, ReduktoService::class.java).apply {
                action = ReduktoService.ACTION_START
            }
            startService(startingIntent)
            val bindingIntent = Intent(this, ReduktoService::class.java)
            bindService(bindingIntent, serviceConnection, Context.BIND_AUTO_CREATE)
        }
    }

    override fun onPause() {
        super.onPause()
        if (!keepBound) {
            unbindService(serviceConnection)
            val stoppingIntent = Intent(this, ReduktoService::class.java).apply {
                action = ReduktoService.ACTION_STOP
            }
            startService(stoppingIntent)
        }
    }
}