package org.bitbucket.asgusev.redukto.android.ui

import android.content.*
import android.service.quicksettings.Tile
import android.service.quicksettings.TileService
import org.bitbucket.asgusev.redukto.android.service.ReduktoService


class ReduktoTileService : TileService() {
    companion object {
        const val ACTION_TOGGLE = "toggle"
        const val EXTRA_ACTIVE = "active"
    }

    var active = false

    override fun onClick() {
        val toggleIntent = Intent(this, ReduktoService::class.java).apply {
            action = ReduktoService.ACTION_TOGGLE
            putExtra(ReduktoService.EXTRA_TOGGLE_TILE, true)
        }
        startForegroundService(toggleIntent)
    }

    override fun onStartListening() {
        qsTile?.state = if (active) Tile.STATE_ACTIVE else Tile.STATE_INACTIVE
        qsTile?.updateTile()
    }

    override fun onStartCommand(intent: Intent?, flags: Int, startId: Int): Int {
        if (intent?.action == ACTION_TOGGLE) {
            active = intent.getBooleanExtra(EXTRA_ACTIVE, false)
            onStartListening()
        }
        return super.onStartCommand(intent, flags, startId)
    }
}
