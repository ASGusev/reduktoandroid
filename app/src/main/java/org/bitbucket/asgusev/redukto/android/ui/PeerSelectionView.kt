package org.bitbucket.asgusev.redukto.android.ui

import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Button
import android.widget.EditText
import android.widget.TextView
import androidx.preference.PreferenceManager
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import org.bitbucket.asgusev.redukto.R
import org.bitbucket.asgusev.redukto.android.service.splitName
import org.bitbucket.asgusev.redukto.protocol.Peer


private const val VIEW_TYPE_HEADER = 0
private const val VIEW_TYPE_IP = 1
private const val VIEW_TYPE_PEER = 2
private const val VIEW_TYPE_PLACEHOLDER = 3
private const val POSITION_HEADER_IP = 0
private const val POSITION_IP_TRANSFER = 1
private const val POSITION_HEADER_PEERS = 2
private const val N_FIXED_ITEMS = 3


class PeerSelectionView(
    view: RecyclerView,
    private val context: Context,
    private val peersList: MutableList<Peer>,
    private val selectionListener: (address: String, userName: String?) -> Unit
) {
    private var addressField: EditText? = null

    private val peersListAdapter = object: RecyclerView.Adapter<RecyclerView.ViewHolder>() {
        inner class HeaderViewHolder(view: View) : RecyclerView.ViewHolder(view) {
            constructor(parent: ViewGroup) : this(
                LayoutInflater.from(parent.context)
                    .inflate(R.layout.header_view, parent, false)
            )

            fun setText(text: String) {
                itemView.findViewById<TextView>(R.id.header_text).text = text
            }
        }

        inner class PeerViewHolder(val view: View) : RecyclerView.ViewHolder(view)

        inner class IPTransferViewHolder(view: View) : RecyclerView.ViewHolder(view)

        inner class PlaceholderViewHolder(view: View) : RecyclerView.ViewHolder(view) {
            constructor(parent: ViewGroup) : this(
                LayoutInflater.from(parent.context)
                    .inflate(R.layout.peer_placeholder_view, parent, false)
            )
        }

        override fun getItemViewType(position: Int) =
            when {
                position == POSITION_HEADER_IP -> VIEW_TYPE_HEADER
                position == POSITION_IP_TRANSFER -> VIEW_TYPE_IP
                position == POSITION_HEADER_PEERS -> VIEW_TYPE_HEADER
                (position == N_FIXED_ITEMS) && peersList.isEmpty() -> VIEW_TYPE_PLACEHOLDER
                else -> VIEW_TYPE_PEER
            }

        override fun onCreateViewHolder(parent: ViewGroup, viewType: Int) =
            when (viewType) {
                VIEW_TYPE_HEADER -> HeaderViewHolder(parent)
                VIEW_TYPE_IP -> {
                    val ipTransferView = LayoutInflater.from(parent.context)
                        .inflate(R.layout.ip_transfer_view, parent, false)
                    addressField = ipTransferView.findViewById(R.id.addressField)
                    loadAddress()
                    val ipTransferButton = ipTransferView.findViewById<Button>(R.id.sendByAddressButton)
                    ipTransferButton.setOnClickListener { selectionListener(addressField!!.text.toString(), null) }
                    IPTransferViewHolder(ipTransferView)
                }
                VIEW_TYPE_PLACEHOLDER -> PlaceholderViewHolder(parent)
                else -> PeerViewHolder(
                    LayoutInflater.from(parent.context)
                        .inflate(R.layout.peer_list_item, parent, false)
                )
            }

        override fun onBindViewHolder(holder: RecyclerView.ViewHolder, position: Int) {
            when {
                position == POSITION_HEADER_IP -> (holder as HeaderViewHolder).setText(context.getString(R.string.ip_transfer))
                position == POSITION_HEADER_PEERS -> (holder as HeaderViewHolder).setText(context.getString(R.string.peers))
                (position >= N_FIXED_ITEMS) && peersList.isNotEmpty() -> {
                    (holder as PeerViewHolder).view.apply {
                        val peer = peersList[position - N_FIXED_ITEMS]
                        val (userName, deviceName) = splitName(peer.name)
                        findViewById<TextView>(R.id.userNameField).text = userName
                        findViewById<TextView>(R.id.deviceNameField).text = deviceName
                        setOnClickListener {
                            selectionListener(peer.address.toString().substring(1), userName)
                        }
                    }
                }
            }
        }

        override fun getItemCount() = N_FIXED_ITEMS + if (peersList.isEmpty()) 1 else peersList.size
    }

    init {
        view.layoutManager = LinearLayoutManager(context)
        view.adapter = peersListAdapter
    }

    fun loadAddress() {
        val preferences = PreferenceManager.getDefaultSharedPreferences(context)
        if (preferences.getBoolean(context.getString(R.string.preference_key_save_ip), true)) {
            val targetIP = preferences.getString(
                context.getString(R.string.preference_key_target_ip), "")
            addressField?.setText(targetIP)
        }
    }

    fun saveAddress() {
        val preferences = PreferenceManager.getDefaultSharedPreferences(context)
        if (preferences.getBoolean(context.getString(R.string.preference_key_save_ip), true))
            with(preferences.edit()) {
                addressField?.let {
                    putString(
                        context.getString(R.string.preference_key_target_ip),
                        it.text.toString()
                    )
                }
                apply()
            }
    }

    fun update() = peersListAdapter.notifyDataSetChanged()
}
