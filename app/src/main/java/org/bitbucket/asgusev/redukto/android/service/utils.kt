package org.bitbucket.asgusev.redukto.android.service

import android.content.ContentValues
import android.content.Context
import android.content.pm.PackageManager
import android.net.Uri
import android.os.Build
import android.os.Environment
import android.provider.MediaStore
import android.provider.OpenableColumns
import androidx.annotation.RequiresApi
import androidx.core.content.ContextCompat
import androidx.core.content.FileProvider
import androidx.documentfile.provider.DocumentFile
import org.bitbucket.asgusev.redukto.R
import org.bitbucket.asgusev.redukto.android.ui.MainActivity
import org.bitbucket.asgusev.redukto.protocol.DirectoryItem
import org.bitbucket.asgusev.redukto.protocol.PathCollisionFixer
import org.bitbucket.asgusev.redukto.protocol.TransferItem
import java.io.FileInputStream
import java.io.FileOutputStream
import java.io.IOException
import java.nio.ByteBuffer
import java.nio.channels.Channels
import java.nio.channels.FileChannel
import java.nio.channels.ReadableByteChannel
import java.nio.file.Files
import java.nio.file.Path
import java.nio.file.Paths
import java.nio.file.StandardOpenOption


private const val DIR_DOWNLOADS = "Download"
private const val DIR_APP = "Redukto"
private const val DIR_APP_DOWNLOADS = "$DIR_DOWNLOADS/$DIR_APP"


internal interface FileWriter {
    fun onProgress(newBytes: ByteBuffer)
    fun onFinish()
    fun onFailure()
    val fileUri: Uri
}

internal interface SavingPolicy {
    fun declareDir(name: String)
    fun getFileWriter(name: String): FileWriter
}

internal class LegacySavingPolicy(private val context: Context): SavingPolicy {
    @Suppress("DEPRECATION")
    private val pathCollisionFixer = PathCollisionFixer(
        Paths.get(Environment.getExternalStoragePublicDirectory(Environment.DIRECTORY_DOWNLOADS).toString())
            .resolve(DIR_APP)
    )

    override fun declareDir(name: String) {
        if (Paths.get(name).nameCount == 1)
            pathCollisionFixer.declareDir(name)
    }

    override fun getFileWriter(name: String): FileWriter {
        if (ContextCompat.checkSelfPermission(context, MainActivity.WRITE_PERMISSION) == PackageManager.PERMISSION_DENIED)
            throw IOException(context.getString(R.string.no_storage_permission))
        val filePath = pathCollisionFixer.fixPath(Paths.get(name))
        if (!filePath.parent.toFile().isDirectory)
            filePath.parent.toFile().mkdirs()
        val fileChannel = FileChannel.open(filePath, StandardOpenOption.WRITE, StandardOpenOption.CREATE)
        return object: FileWriter {
            override fun onProgress(newBytes: ByteBuffer) {
                try {
                    fileChannel.write(newBytes)
                } catch (error: IOException) {
                    onFailure()
                    throw error
                }
            }

            override fun onFinish() {
                fileChannel.close()
            }

            override fun onFailure() {
                try {
                    fileChannel.close()
                } catch (e: IOException) {}
                try {
                    Files.delete(filePath)
                } catch (e: IOException) {}
            }

            override val fileUri: Uri
                get() = FileProvider.getUriForFile(context, context.packageName, filePath.toFile())
        }
    }
}

@RequiresApi(Build.VERSION_CODES.Q)
internal class ScopedSavingPolicy(private val context: Context): SavingPolicy {
    private val dirNameMapping = mutableMapOf<String, String>()

    private fun makeMapping(srsDirName: String) {
        val cursor = context.contentResolver.query(
            MediaStore.Downloads.EXTERNAL_CONTENT_URI,
            arrayOf(MediaStore.Downloads.RELATIVE_PATH),
            MediaStore.Downloads.RELATIVE_PATH + " like ?", arrayOf("$DIR_APP_DOWNLOADS/$srsDirName%"),
            null
        )
        val existingDirs = mutableSetOf<String>()
        cursor?.use {
            cursor.moveToFirst()
            for (i in 1..cursor.count) {
                val relPath = Paths.get(cursor.getString(0))
                if (relPath.nameCount > 2)
                    existingDirs.add(relPath.getName(2).toString())
                cursor.moveToNext()
            }
        }
        var tgtDirName = srsDirName
        var step = 1
        while (tgtDirName in existingDirs) {
            tgtDirName = "$srsDirName ($step)"
            ++step
        }
        dirNameMapping[srsDirName] = tgtDirName
    }

    private fun remapPath(srcPath: Path): Path {
        val tgtFirstDir: String = dirNameMapping[srcPath.getName(0).toString()]!!
        return Paths.get(tgtFirstDir).resolve(srcPath.subpath(1, srcPath.nameCount))
    }

    override fun declareDir(name: String) {
        val path = Paths.get(name)
        if (path.nameCount == 1)
            makeMapping(name)
    }


    override fun getFileWriter(name: String): FileWriter {
        val filePath = Paths.get(name)
        val targetParent = if (filePath.nameCount == 1) {
            DIR_APP_DOWNLOADS
        } else {
            val remappedPath = remapPath(filePath)
            "$DIR_APP_DOWNLOADS/${remappedPath.parent}"
        }

        return object: FileWriter {
            private val fileDesc = ContentValues().apply {
                put(MediaStore.Downloads.DISPLAY_NAME, filePath.fileName.toString())
                put(MediaStore.Downloads.RELATIVE_PATH, targetParent)
                put(MediaStore.Downloads.IS_PENDING, 1)
            }
            private val contentResolver = context.contentResolver

            override val fileUri = contentResolver.insert(MediaStore.Downloads.EXTERNAL_CONTENT_URI, fileDesc)!!

            private val parcelFileDescriptor = contentResolver.openFileDescriptor(fileUri, "w")

            private val fileChannel = FileOutputStream(parcelFileDescriptor!!.fileDescriptor).channel

            override fun onProgress(newBytes: ByteBuffer) {
                try {
                    fileChannel.write(newBytes)
                } catch (error: IOException) {
                    onFailure()
                    throw error
                }
            }

            override fun onFailure() {
                try {
                    fileChannel.close()
                    parcelFileDescriptor?.close()
                } catch (error: IOException) {}
                contentResolver.delete(fileUri, null, null)
            }

            override fun onFinish() {
                try {
                    fileChannel.close()
                    parcelFileDescriptor?.close()
                } finally {
                    fileDesc.put(MediaStore.Downloads.IS_PENDING, 0)
                    contentResolver.update(fileUri, fileDesc, null, null)
                }
            }
        }
    }
}

internal fun getSavingPolicy(context: Context): SavingPolicy =
    if (Build.VERSION.SDK_INT <= Build.VERSION_CODES.Q)
        LegacySavingPolicy(context)
    else
        ScopedSavingPolicy(context)


fun splitName(name: String): Pair<String, String> {
    val posAt = name.indexOf(" at ")
    val posBrace = name.lastIndexOf(" (")
    val userName = name.substring(0, posAt)
    val deviceName = name.substring(posAt + 4, posBrace)
    return Pair(userName, deviceName)
}

class UriFileItem(uri: Uri, context: Context, prefix: Path = Paths.get("")) : TransferItem {
    private val descriptor = context.contentResolver.openFileDescriptor(uri, "r")!!

    override val size = descriptor.statSize

    override val header = prefix.resolve(getName(uri, context)).toString()

    private fun getName(uri: Uri, context: Context): String =
        context.contentResolver.query(uri, null, null, null).use {
            it?.moveToFirst()
            val columnIndex = it?.getColumnIndex(OpenableColumns.DISPLAY_NAME) ?: -1
            if (columnIndex < 0)
                return uri.pathSegments.last()
            return it?.getString(columnIndex) ?: uri.pathSegments.last()
        }

    override fun getChannel(): ReadableByteChannel =
        Channels.newChannel(FileInputStream(descriptor.fileDescriptor))
}


fun collectDirectory(directory: DocumentFile, context: Context, prefix: Path = Paths.get("")): List<TransferItem> {
    val childPrefix = prefix.resolve(directory.name)
    val dirItem = DirectoryItem(childPrefix)
    val childrenItems = directory.listFiles().toList().flatMap {
        if (it.isDirectory)
            collectDirectory(it, context, childPrefix)
        else
            listOf(UriFileItem(it.uri, context, childPrefix))
    }
    return listOf(dirItem) + childrenItems
}


fun checkAddress(address: String): Boolean {
    if (address.any { !it.isDigit() && it != '.' })
        return false
    val parts = address.split('.')
    if (parts.size != 4)
        return false
    return parts.all { it.length <= 3 && it.toLong() < 256 }
}
