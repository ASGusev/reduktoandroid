package org.bitbucket.asgusev.redukto.android.ui

import android.os.Bundle
import android.view.MenuItem
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import androidx.preference.*
import org.bitbucket.asgusev.redukto.R

class SettingsActivity : AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.settings_activity)
        title = getString(R.string.settings)
        if (savedInstanceState == null) {
            supportFragmentManager
                .beginTransaction()
                .replace(R.id.settings, SettingsFragment())
                .commit()
        }
        supportActionBar?.setDisplayHomeAsUpEnabled(true)
    }

    class SettingsFragment : PreferenceFragmentCompat() {
        override fun onCreatePreferences(savedInstanceState: Bundle?, rootKey: String?) {
            setPreferencesFromResource(R.xml.root_preferences, rootKey)
            val usernamePreference = findPreference<EditTextPreference>(getString(R.string.preference_key_username))
            usernamePreference?.setOnPreferenceChangeListener { _, newValue ->
                if ((newValue as String).isEmpty()) {
                    Toast.makeText(this.context, getString(R.string.username_empty), Toast.LENGTH_SHORT).show()
                    usernamePreference.text = getString(R.string.default_username)
                    return@setOnPreferenceChangeListener false
                }
                if (newValue.contains(' ')) {
                    Toast.makeText(this.context, getString(R.string.username_spaces), Toast.LENGTH_SHORT).show()
                    return@setOnPreferenceChangeListener false
                }
                return@setOnPreferenceChangeListener true
            }

            val saveIPPreference = findPreference<SwitchPreferenceCompat>(getString(R.string.preference_key_save_ip))
            saveIPPreference?.setOnPreferenceChangeListener { _, newValue ->
                if (!(newValue as Boolean))
                    with (PreferenceManager.getDefaultSharedPreferences(requireContext()).edit()) {
                        remove(getString(R.string.preference_key_save_ip))
                        apply()
                    }
                return@setOnPreferenceChangeListener true
            }
        }
    }

    override fun onOptionsItemSelected(item: MenuItem) =
        when (item.itemId) {
            android.R.id.home -> {
                finish()
                true
            }
            else -> super.onOptionsItemSelected(item)
        }
}